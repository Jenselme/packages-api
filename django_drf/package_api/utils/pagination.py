from rest_framework.pagination import PageNumberPagination as DjangoPageNumberPagination


class PageNumberPagination(DjangoPageNumberPagination):
    page_size = 500
    max_page_size = 1_000
