from rest_framework import serializers

from .models import Package


class PackageSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Package
        fields = ('id', 'created_at', 'name', 'owner')
