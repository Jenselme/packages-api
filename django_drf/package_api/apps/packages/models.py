from django.db import models


class Package(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, unique=True)
    owner = models.ForeignKey('core.User', on_delete=models.CASCADE)
