from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .models import Package
from .serializer import PackageSerializer


class ListCreatePackage(ListCreateAPIView):
    name = 'create_retrieve_package'
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = PackageSerializer
    queryset = Package.objects.all().order_by('id')

    def get_serializer(self, *args, data=None, **kwargs):
        if data:
            data = data.copy()
            data['owner'] = self.request.user.pk
            return super().get_serializer(*args, data=data, **kwargs)

        return super().get_serializer(*args, **kwargs)

    def get_queryset(self):
        # Let's keep things simple and don't go into Generic filtering: https://www.django-rest-framework.org/api-guide/filtering/#generic-filtering
        qs = super().get_queryset()
        if name := self.request.query_params.get('name'):
            print(name)
            qs = qs.filter(name__icontains=name)
        return qs
