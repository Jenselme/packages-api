from django.urls import path

from . import views

app_name = 'packages'

urlpatterns = [
    path('packages/', views.ListCreatePackage.as_view(), name=views.ListCreatePackage.name),
]
