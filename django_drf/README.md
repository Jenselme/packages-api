# Django & Django Rest Framework version

To launch project:
1. Install Python and [Pipenv](https://github.com/pypa/pipenv#installation).
2. Install project deps with `pipenv install --dev`.
3. Run migrations: `pipenv run ./manage.py migrate`
4. Run dev server: `pipenv run ./manage.py runserver`

If you want to create an admin user, run `pipenv run ./manage.py createsuperuser`

To use the project (relies on [httpie](https://httpie.io/), you can use `curl` instead):
- Create a user: `http POST localhost:8000/auth/registration/ <<< '{"username": "jujens", "password1": "6Sz3i7HBnp3J&QKm", "password2": "6Sz3i7HBnp3J&QKm", "email": "jujens@jujens.eu"}'` You will get a token back, like this:
  ```json
  {
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5MjgzNzI1LCJqdGkiOiJlZTYyZTgwNzY1ODA0NzBiYWIzYTI4ODY2ZWQ2ZDAxOCIsInVzZXJfaWQiOjF9.J0_UQJH2OMV7mOkSCe2kWiOQyZv4CMKsAdGAU_2vlyI",
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxOTM2OTgyNSwianRpIjoiYTVkN2Y1OTk2Nzc1NDQ0NGIzNzJhNjczZjQwOWZmMTkiLCJ1c2VyX2lkIjoxfQ.9Q26LraBhvnDVj3bdErHO-a3-ZU4KMmGgGT_uIJAc-E",
    "user": {
        "email": "jujens@jujens.eu",
        "first_name": "",
        "last_name": "",
        "pk": 1,
        "username": "jujens"
    }
  }
  ```
- To get a token: `http POST localhost:8000/auth/login/ <<< '{"username": "jujens", "password": "6Sz3i7HBnp3J&QKm"}'`
- To create a package: `http -h POST localhost:8000/api/packages/ 'Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5MjgzNzI1LCJqdGkiOiJlZTYyZTgwNzY1ODA0NzBiYWIzYTI4ODY2ZWQ2ZDAxOCIsInVzZXJfaWQiOjF9.J0_UQJH2OMV7mOkSCe2kWiOQyZv4CMKsAdGAU_2vlyI' <<< ''`
- To list package: `http localhost:8000/api/packages/`

**Note:** All those tokens and password were randomly generated for the purpose of this file and to ease testing with copy/pasting. I don't use them anywhere. 
