(ns clj-luminus.routes.api
  (:require
    [struct.core :as st]
    [ring.util.http-response :as response]
    [muuntaja.core :refer [encode create]]
    [clj-luminus.db.core :as db]
    [clj-luminus.middleware :as middleware]))

(def serializer (create))

(def create-package-schema
  [[:name st/required st/string]])

(defn respond-with-json [data]
  (-> (response/ok (encode serializer "application/json" data))
      (response/header "Content-Type" "application/json; charset=utf-8")))

(defn respond-with-error [message]
  (-> (response/bad-request (encode serializer "application/json" {:error message}))
      (response/header "Content-Type" "application/json; charset=utf-8")))

(defn list-packages [{:keys [params]}]
  (if params
    (respond-with-json (db/list-all-packages-filter-name {:name-like (str "%" (:name params) "%")}))
    (respond-with-json (db/list-all-packages))))

(defn insert-package! [data-to-insert]
  (try
    (db/create-package! data-to-insert)
    (catch Exception e nil)))

(defn create-package-from-validated-data! [params]
  (let [data-to-insert (assoc params :owner-id 1 :created-at (java.util.Date.))
        data (insert-package! data-to-insert)
        id (second (into [] cat (first data)))
        row (assoc data-to-insert :id id)]
    (if (= id nil)
      (respond-with-error "Duplicated row, cannot insert.")
      (respond-with-json row))))

(defn create-package! [{:keys [params]}]
  (let [[validation-errors validated-params] (st/validate params create-package-schema {:strip true})]
    (println validation-errors validated-params (= nil validation-errors))
    (if (nil? validation-errors)
      (create-package-from-validated-data! validated-params)
      (respond-with-error validation-errors))))

(defn api-routes []
  [""
   {:middleware [middleware/wrap-formats]}
   ["/api/packages" {:get list-packages :post create-package!}]])
