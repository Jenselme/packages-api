# clj_luminus

Generated using Luminus version "4.10" with the command `lein new luminus clj_luminus +sqlite`

FIXME

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

You will need [Clojure](https://clojure.org/) and [leinigen](https://leiningen.org/) installed. 

To start a web server for the application:

1. Launch the migrations with: `lein run migrate`
2. Run the project with: `lein run` 

## Usage

- To create a package: `http POST localhost:3000/api/packages <<< '{"name": "Test create 5"}'`
- To list all packages: `http localhost:3000/api/packages`
- To filter packages by names: `http 'localhost:3000/api/packages?name=Super`

## License

Copyright © 2021 FIXME
