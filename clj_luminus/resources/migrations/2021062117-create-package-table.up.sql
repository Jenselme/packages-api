CREATE TABLE "packages_package"
(
    "id"         integer      NOT NULL PRIMARY KEY AUTOINCREMENT,
    "created_at" datetime     NOT NULL,
    "name"       varchar(100) NOT NULL UNIQUE,
    "owner_id"   bigint       NOT NULL REFERENCES "core_user" ("id") DEFERRABLE INITIALLY DEFERRED
);


CREATE INDEX "packages_package_owner_id_48dfe445" ON "packages_package" ("owner_id");
