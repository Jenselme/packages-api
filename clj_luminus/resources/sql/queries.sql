-- :name list-all-packages :? :*
-- :doc Select all packages
SELECT * FROM packages_package

-- :name list-all-packages-filter-name :? :*
-- :doc List all packages that have the supplied parameter in the same.
SELECT * FROM packages_package WHERE name LIKE :name-like

-- :name create-package! :i! :raw
-- :doc Create a new package.
INSERT INTO packages_package (created_at, name, owner_id) VALUES (:created-at, :name, :owner-id)