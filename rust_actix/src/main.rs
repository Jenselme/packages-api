use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use sqlx::sqlite::SqlitePool;
use std::env;

mod packages;

use packages::{Package, CreatePackageRequest, SearchPackageRequest};

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[get("/packages")]
async fn list_packages(filter_request: web::Query<SearchPackageRequest>, db_pool: web::Data<SqlitePool>) -> impl Responder {
    let result = match filter_request.into_inner().name{
        None => Package::find_all(db_pool.get_ref()).await,
        Some(name) => Package::filter_by_name(name, db_pool.get_ref()).await,
    };
    match result {
        Ok(packages) => HttpResponse::Ok().json(packages),
        Err(error) => {
            println!("{:?}", error);
            HttpResponse::BadRequest().body("Error while reading the database.")
        },
    }
}

#[post("/packages")]
async fn create_package(package_request: web::Json<CreatePackageRequest>, db_pool: web::Data<SqlitePool>) -> impl Responder {
    let result = Package::create(package_request.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(package) => HttpResponse::Ok().json(package),
        Err(error) => {
            println!("{:?}", error);
            HttpResponse::BadRequest().body(get_create_package_error_message(error))
        }
    }
}

fn get_create_package_error_message(error: anyhow::Error) -> &'static str {
    if error.to_string().contains("UNIQUE constraint failed") {
        return "Duplicated package name."
    }

    return "Error while creating the package.";
}

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL env var must be set.");
    let db_pool = SqlitePool::connect(&database_url).await?;

    HttpServer::new(move || {
        App::new()
            .data(db_pool.clone())
            .service(hello)
            .service(list_packages)
            .service(create_package)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await?;

    Ok(())
}
