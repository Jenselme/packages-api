use actix_web::{Error, HttpRequest, HttpResponse, Responder};
use futures::future::{ready, Ready};
use anyhow::Result;
use serde::{Serialize, Deserialize};
use sqlx::{FromRow, SqlitePool, Row};
use sqlx::sqlite::SqliteRow;
use chrono::{self, offset::TimeZone, DateTime, Utc};

type SqlDateTime = DateTime<Utc>;

#[derive(Serialize, FromRow)]
pub struct Package {
    pub id: i64,
    pub name: String,
    pub created_at: SqlDateTime,
}

#[derive(Serialize, Deserialize)]
pub struct CreatePackageRequest {
    pub name: String,
}

#[derive(Serialize, Deserialize)]
pub struct SearchPackageRequest {
    pub name: Option<String>,
}

impl Responder for Package {
    type Error = Error;
    type Future = Ready<Result<HttpResponse, Error>>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self).unwrap();

        ready(Ok(HttpResponse::Ok().content_type("application/json").body(body)))
    }
}

impl Package { 
    pub async fn find_all(pool: &SqlitePool) -> Result<Vec<Package>> {
        let mut packages = vec![];
        let recs = sqlx::query!(
            r#"SELECT id, name, created_at
                FROM packages_package
                ORDER BY id
            "#
        )
            .fetch_all(pool)
            .await?;

        for rec in recs {
            packages.push(Package {
                id: rec.id,
                name: rec.name,
                created_at: Utc.from_local_datetime(&rec.created_at).unwrap(),
            });
        }

        Ok(packages)
    }

    pub async fn filter_by_name(name: String, pool: &SqlitePool) -> Result<Vec<Package>> {
        let mut packages = vec![];
        let mut like_query = "%".to_owned();
        like_query.push_str(&name);
        like_query.push('%');

        let recs = sqlx::query!(
            r#"SELECT id, name, created_at
                FROM packages_package
                WHERE name LIKE ?
                ORDER BY id
            "#,
            like_query
        )
            .fetch_all(pool)
            .await?;

        for rec in recs {
            packages.push(Package {
                id: rec.id,
                name: rec.name,
                created_at: Utc.from_local_datetime(&rec.created_at).unwrap(),
            });
        }

        Ok(packages)
    }

    pub async fn create(package_request: CreatePackageRequest, pool: &SqlitePool) -> Result<Package> {
        let mut tx = pool.begin().await?;
        let package = sqlx::query("INSERT INTO packages_package (name, created_at, owner_id) VALUES ($1, $2, $3) RETURNING id, created_at, name")
            .bind(package_request.name)
            .bind(Utc::now())
            // Owner is currently hard coded.
            .bind(1)
            .map(|row: SqliteRow| {
                Package {
                    id: row.get(0),
                    created_at: row.get(1),
                    name: row.get(2),
                }
            })
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await?;

        Ok(package)
    }
}
