--
-- Create model User
--
CREATE TABLE "core_user"
(
    "id"           integer      NOT NULL PRIMARY KEY AUTOINCREMENT,
    "password"     varchar(128) NOT NULL,
    "last_login"   datetime NULL,
    "is_superuser" bool         NOT NULL,
    "username"     varchar(150) NOT NULL UNIQUE,
    "first_name"   varchar(150) NOT NULL,
    "last_name"    varchar(150) NOT NULL,
    "email"        varchar(254) NOT NULL,
    "is_staff"     bool         NOT NULL,
    "is_active"    bool         NOT NULL,
    "date_joined"  datetime     NOT NULL
);

-- Create a default user.
INSERT INTO "core_user" ("password", "last_login", "is_superuser", "username", "first_name",
                         "last_name", "email", "is_staff", "is_active", "date_joined")
VALUES ('pbkdf2_sha256$260000$XSNpHgb9NMjvIyKoZhA00A$fohjjJn8S/0OdjmdYyMoeNPCP00iuHLJ0Z4b2o5y080=',
        NULL,
        true,
        'jujens',
        '',
        '',
        'jujens@jujens.eu',
        true,
        true,
        CURRENT_TIMESTAMP);
