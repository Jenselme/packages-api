# Rust actix

To launch project:
1. Install Rust and cargo. I suggest you do this with [rustup](https://rustup.rs/).
2. Create the database: `./bin/create-database.sh`
3. Run the project with `DATABASE_URL=sqlite:packages.db cargo run`.

To use the project (relies on [httpie](https://httpie.io/), you can use `curl` instead):
- To list all packages: `http localhost:8080/packages`
- To create a package: `http POST localhost:8080/packages <<< '{"name": "Test create"}'`
- To filter packages: `http 'localhost:8080/packages?name=create'`