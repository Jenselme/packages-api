#!/usr/bin/env bash

set -e
set -u

sqlite3 packages.db < sql/create_user_table.sql
sqlite3 packages.db < sql/create_package_table.sql
